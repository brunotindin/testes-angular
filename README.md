# DOCS - TESTES NO ANGULAR COM JASMINE 

Documentação e passo a passo para testes no angular, utilizando as ferramentas Jasmine e Karma
# Wiki
[DOCS - Jasmine - EN-US](https://jasmine.github.io/tutorials/your_first_suite)  
[DOCS - Angular sobre testes - EN-US](https://angular.io/guide/testing)  
[DOCS - Angular Testando Services - EN-US](https://angular.io/guide/testing-services)  
[DOCS - Angular Testes Http Requests - EN-US](https://angular.io/guide/http#testing-http-requests)  
\
[Curso - projetos Angular 12, com testes básicos](https://www.udemy.com/course/formacao-angular-inicio-criando-7-projetos/learn/lecture/7071622?start=150#overview)  
[Curso - Testes com Jasmine em JS](https://www.udemy.com/course/aprenda-testes-unitarios-com-jasmine-javascript/learn/lecture/6435224?start=0#overview)  
\
[Artigo sobre testes no Angular - PT-BR](https://ichi.pro/pt/angular-teste-de-unidade-jasmine-karma-passo-a-passo-249827192539828)  
[Artigo Medium Testes em Angular - PT-BR](https://mjsjunior.medium.com/testes-unit%C3%A1rios-em-sua-aplica%C3%A7%C3%A3o-angular-7838487c6bd3)  
[Artigo Medium Testes em Angular com HTTP - PT-BR](https://mjsjunior.medium.com/testando-services-e-requisi%C3%A7%C3%B5es-http-em-aplica%C3%A7%C3%B5es-angular-50f7051cae72)  

# Comandos Básicos Jasmine

```describe (string, function) — Cria um grupo de testes, sua suite de testes. Pode ser aninhado para compor diversos contextos. Seu primeiro parâmetro é o nome da suite e o segundo é a função```  

ex:

    describe("Boas vindas", function() {
        let name;

        it("deve retornar Oi, Joao", function() {
            name = 'Joao';

            expect(digaOi(name)).toBeEqual('Oi, Joao');
        });
        });  
```it (string, function) — Define um único teste. Um teste deve conter uma ou mais expectativas que testem o estado do código.```  

ex: 

        it("deve retornar Oi, Joao", function() {
                name = 'Joao';

                expect(digaOi(name)).toBeEqual('Oi, Joao');
            });
```expect(any) — Cria uma expectativa. No exemplo, nossa expectativa é que o retorno da função seja igual a ‘Oi, João```  
ex:
    
    expect(digaOi(name)).toBeEqual('Oi, Joao');
    

```beforeEach — Um dos objetivos dos testes unitários é testar individualmente a menor parte possível do sistema, a função beforeEach será executada antes de cada um dos blocos de testes que escrevermos, assim, garantimos a inicialização correta e limpa das variáveis utilizadas em nossos testes.```  
ex:
  
        beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeopleListComponent ]
    });
  

# Angular TestBed(ATB)
###### - Angular TesteBed é responsável pela configuração dos ambientes de teste que dependem do Angular, assim, ganhamos facilidade para testarmos o nossos componentes e serviços.  
\
A configuração do ATB é exatamente da mesma forma que configuramos o nosso NgModule.
 Ex:

    beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeopleListComponent ],
      providers: [ExmploService]
    });

## Quando usar o ATB ?

###### - Deve ser utilizado sempre que for necessário realizar alguma manipulação do DOM, simular interações de usuários, detecções de mudanças, e injeção de dependências.

# Observações

###### - Uma Boa Prática é começar a descrição do teste com 'Should' ou 'Deve', expressando o que deve acontecer no teste.

# Testes Comandos
###### - Para começar os testes no angular:

1. Inicie todos os arquivos de testes 
```bash
ng test
```
ou

2. Inicie um arquivo de teste especifico
```bash
ng test --main src/path-do-arquivo.spec.ts
```